package org.munaylab.lotengo

enum Estado {
    PENDIENTE,
    ENVIADO,
    RECIBIDO,
    PERDIDO

    String clase() {
        if (this == PENDIENTE) return 'info'
        if (this == ENVIADO) return 'warning'
        if (this == RECIBIDO) return 'success'
        if (this == PERDIDO) return 'danger'
    }

    String icono() {
        if (this == PENDIENTE) return 'fa-clock-o'
        if (this == ENVIADO) return 'fa-paper-plane'
        if (this == RECIBIDO) return 'fa-check'
        if (this == PERDIDO) return 'fa-times'
    }

    static Estado de(String nombre) {
        String estado = nombre.toUpperCase()
        if (estado == 'ENVIADO') {
            return ENVIADO
        } else if (estado == 'RECIBIDO') {
            return RECIBIDO
        } else if (estado == 'PERDIDO') {
            return PERDIDO
        } else {
            return PENDIENTE
        }
    }

}
