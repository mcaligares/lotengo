package org.munaylab.lotengo

class UtilTagLib {
    static defaultEncodeAs = [enviosPendientes:'html']
    static returnObjectForTags = ['configuracionActual']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    def gestionService
    def springSecurityService

    def configuracionActual = {
        springSecurityService.currentUser.configuracion
    }

    def enviosPendientes = { attrs, body ->
        def envios = gestionService.enviosPendientes()
        out << render(template: '/componentes/enviosPendientes', model: [envios: envios])
    }

    def ultimosMovimientos = { attrs, body ->
        def envios = gestionService.ultimosMovimientos()
        out << render(template: '/componentes/ultimosMovimientos', model: [envios: envios])
    }

    def ultimosPaquetes = { attrs, body ->
        Estado estado = Estado.de(attrs.estado)
        def paquetes = gestionService.ultimosPaquetes(estado)
        out << render(template: '/componentes/ultimosPaquetes',
            model: [estado: estado, paquetes: paquetes])
    }

    def agregarPaquete = { attrs, body ->
        def ubicaciones = gestionService.todasLasUbicaciones()
        out << render(template: '/componentes/agregarPaquete', model: [ubicaciones: ubicaciones])
    }

    def resumenPaquetes = { attrs, body ->
        def resumen = gestionService.resumenPaquetes()
        out << render(template: '/componentes/resumen', model: [resumen: resumen])
    }

    def enviosCaducados = { attrs, body ->
        def envios = gestionService.enviosCaducados()
        out << render(template: '/componentes/enviosCaducados', model: [envios: envios])
    }
}
