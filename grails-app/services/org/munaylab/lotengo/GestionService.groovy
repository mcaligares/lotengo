package org.munaylab.lotengo

import groovy.time.TimeCategory
import grails.gorm.transactions.Transactional

@Transactional
class GestionService {

    def springSecurityService

    def actualizarUbicacion(Ubicacion command) {
        if (command.validate()) {
            command.save()
        }
        return command
    }

    def todasLasUbicaciones() {
        Ubicacion.listOrderByProvincia(order: 'asc')
    }

    def enviosPendientes() {
        enviosPendientes(springSecurityService.currentUser)
    }
    def enviosPendientes(Usuario usuario) {
        Historial.createCriteria().list {
            paquete {
                eq 'estado', Estado.ENVIADO
                destino {
                    ilike 'provincia', usuario.sede
                }
            }
        }
    }

    def ultimosMovimientos() {
        ultimosMovimientos(springSecurityService.currentUser)
    }
    def ultimosMovimientos(Usuario usuario, int max = 10) {
        Historial.listOrderByFecha(max: max, order: 'desc')
    }

    def enviosCaducados() {
        enviosCaducados(springSecurityService.currentUser)
    }
    def enviosCaducados(Usuario usuario) {
        Historial.createCriteria().list {
            eq 'estado', Estado.ENVIADO
            paquete {
                eq 'estado', Estado.ENVIADO
                destino {
                    ilike 'provincia', usuario.sede
                }
            }
            sqlRestriction 'TIMESTAMPDIFF(MONTH, fecha, NOW()) >= 1'
        }
    }

    def paqueteEnviado(Long id, String observacion = null, Date fecha = new Date()) {
        actualizarEstado(id, Estado.ENVIADO, observacion, fecha)
    }
    def paqueteRecibido(Long id, String observacion = null, Date fecha = new Date()) {
        actualizarEstado(id, Estado.RECIBIDO, observacion, fecha)
    }
    def paquetePerdido(Long id, String observacion = null, Date fecha = new Date()) {
        actualizarEstado(id, Estado.PERDIDO, observacion, fecha)
    }
    def actualizarEstado(Long id, Estado estado, String observacion = null, Date fecha = new Date()) {
        Paquete paquete = Paquete.get(id)
        if (!paquete) return
        paquete.estado = estado
        paquete.save()
        new Historial(responsable: springSecurityService.currentUser, paquete: paquete,
            fecha: fecha, observacion: observacion, estado: estado).save()
    }

    def actualizarPaquete(Paquete command) {
        if (command.validate()) {
            command.save()
        }
        return command
    }
    def ultimosPaquetes(Estado estado, int max = 10) {
        Paquete.findAllByEstado(estado, [max: max])
    }
    def buscarPaquete(Long numeroCaja) {
        Paquete paquete = Paquete.findByCaja(numeroCaja)
        List<Historial> historial = Historial.findAllByPaquete(paquete)
        [paquete: paquete, historial: historial]
    }

    def resumenPaquetes() {
        Paquete.createCriteria().list {
            projections {
                count()
                property 'estado'
                groupProperty 'estado'
            }
        }
    }

    def actualizarConfiguracion(Configuracion command) {
        Usuario usuario = springSecurityService.currentUser
        if (usuario.configuracion.id != command.id) return null
        usuario.configuracion.actualizar(command)
        return usuario.configuracion.save()
    }
}
