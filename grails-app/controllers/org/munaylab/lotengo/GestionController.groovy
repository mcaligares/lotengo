package org.munaylab.lotengo

class GestionController {

    def gestionService

    def index() {}

    def actualizarUbicacion(Ubicacion command) {
        Ubicacion ubicacion = gestionService.actualizarUbicacion(command)
        if (ubicacion.hasErrors()) {
            flash.result = [tipo: 'danger', texto: command.errors.allErrors]
        }
        redirect uri:'/'
    }
    def eliminarUbicacion(Ubicacion command) {
        command.delete()
    }
    def listarUbicaciones() {
        [ubicaciones: Ubicacion.listOrderByProvincia()]
    }

    def actualizarPaquete(Paquete command) {
        Paquete paquete = gestionService.actualizarPaquete(command)
        if (paquete.hasErrors()) {
            flash.result = [tipo: 'danger', texto: command.errors.allErrors]
        }
        redirect uri:'/'
    }
    def listarPaquetes() {
        [paquetes: Paquete.listOrderByCaja()]
    }
    def buscarPaquete() {
        def result = gestionService.buscarPaquete(params.long('caja'))
        if (result && result.paquete) {
            flash.encontrado = result
        } else {
            flash.result = [tipo: 'danger', texto: 'No se encontró resultado del paquete solicitado.']
        }
        redirect uri: '/'
    }

    def enviar(Long id) {
        Historial enviado = gestionService.paqueteEnviado(id)
        if (enviado) {
            flash.result = [tipo: 'info', texto: "Se cambió el estado del paquete ${enviado.paquete.caja} a ENVIADO"]
        } else {
            flash.result = [tipo: 'danger', texto: 'Ocurrió un problema al cambiar el estado del paquete.']
        }
        redirect uri:'/'
    }

    def recibido(Long id) {
        Date fecha = params.fecha ? new Date().parse('dd/MM/yyyy', params.fecha) : new Date()
        Historial recibido = gestionService.paqueteRecibido(id, params.observacion, fecha)
        if (recibido) {
            flash.result = [tipo: 'info', texto: "Se cambió el estado del paquete ${recibido.paquete.caja} a RECIBIDO"]
        } else {
            flash.result = [tipo: 'error', texto: 'Ocurrió un problema al cambiar el estado del paquete.']
        }
        redirect uri:'/'
    }

    def perdido(Long id) {
        Historial perdido = gestionService.paquetePerdido(id)
        if (perdido) {
            flash.result = [tipo: 'info', texto: "Se cambió el estado del paquete ${perdido.paquete.caja} a PERDIDO"]
        } else {
            flash.result = [tipo: 'error', texto: 'Ocurrió un problema al cambiar el estado del paquete.']
        }
        redirect uri:'/'
    }

    def configuracion(Configuracion command) {
        Configuracion config = gestionService.actualizarConfiguracion(command)
        if (!config) {
            flash.result = [tipo: 'error', texto: 'Ocurrió un problema al actualizar la configuración.']
        }
        redirect uri:'/'
    }
}
