package org.munaylab.lotengo

class Paquete {

    Integer caja
    Estado estado
    Ubicacion origen
    Ubicacion destino
    String descripcion
    Date dateCreated
    Date lastUpdated

    static constraints = {
        caja nullable: false, unique: true
        descripcion nullable: true
    }
    static mapping = {
        origen fetch: 'join'
        destino fetch: 'join'
    }

}
