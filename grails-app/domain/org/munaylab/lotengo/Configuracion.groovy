package org.munaylab.lotengo

class Configuracion {

    Usuario usuario
    boolean resumen
    boolean buscador
    boolean agregarPaquete
    boolean agregarUbicacion
    boolean ultimosMovimientos
    boolean paquetesPendientes
    boolean paquetesEnviados
    boolean paquetesRecibidos
    boolean paquetesPerdidos

    static constraints = {

    }

    static Configuracion getInicial() {
        new Configuracion(resumen: true, buscador: true, agregarPaquete: true,
            ultimosMovimientos: true, paquetesRecibidos: true)
    }

    void actualizar(Configuracion config) {
        resumen = config.resumen == true
        buscador = config.buscador == true
        agregarPaquete = config.agregarPaquete == true
        agregarUbicacion = config.agregarUbicacion == true
        ultimosMovimientos = config.ultimosMovimientos == true
        paquetesPendientes = config.paquetesPendientes == true
        paquetesEnviados = config.paquetesEnviados == true
        paquetesRecibidos = config.paquetesRecibidos == true
        paquetesPerdidos = config.paquetesPerdidos == true
    }
}
