package org.munaylab.lotengo

class Ubicacion {

    String provincia
    String localidad
    String descripcion

    static constraints = {
        descripcion nullable: true
    }

    String toString() {
        "$provincia/$localidad"
    }
}
