package org.munaylab.lotengo

class Historial {

    Date fecha
    Estado estado
    Paquete paquete
    String observacion
    Usuario responsable
    Date dateCreated
    Date lastUpdated

    static constraints = {
        observacion nullable: true
    }

}
