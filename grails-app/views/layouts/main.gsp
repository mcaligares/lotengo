<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="Lo Tengo App"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <asset:stylesheet src="css/bootstrap.min.css"/>
    <asset:stylesheet src="css/font-awesome.min.css"/>
    <asset:stylesheet src="main.css"/>

    <asset:javascript src="jquery-2.2.0.min.js"/>
    <asset:javascript src="bootstrap.min.js"/>

    <g:layoutHead/>
</head>
<body>
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Lo Tengo</a>
      </div>

      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
          <sec:ifAllGranted roles='ADMIN'>
            <li>
              <a id="config" href="#">
                <i class="fa fa-cog" aria-hidden="true"></i>
                Configuración
              </a>
            </li>
          </sec:ifAllGranted>
          <li><a href="#">Contacto</a></li>
          <sec:ifLoggedIn>
            <li><p class="navbar-text"><b><sec:username/></b></p></li>
            <li>
              <form action="/logout" method="post">
                <button type="submit" class="btn btn-default navbar-btn">Salir</button>
              </form>
            </li>
          </sec:ifLoggedIn>
        </ul>
      </div>
    </div>
  </nav>

  <sec:ifAllGranted roles='ADMIN'>
    <section>
      <g:layoutBody/>
    </section>
    <g:render template="/componentes/configuracion"/>
  </sec:ifAllGranted>
  <sec:ifNotGranted roles='ADMIN'>
    <section class="container">
      <g:layoutBody/>
    </section>
  </sec:ifNotGranted>

  <footer class="footer text-center">
    <div class="container">
      <div class="row">
        <div class="col-sm-6 mb-5 mb-lg-0">
          <h4 class="text-uppercase mb-4">Around the Web</h4>
          <ul class="list-inline mb-0">
            <li class="list-inline-item">
              <a class="btn btn-outline-light btn-social text-center rounded-circle" href="#">
                <i class="fa fa-fw fa-facebook"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a class="btn btn-outline-light btn-social text-center rounded-circle" href="#">
                <i class="fa fa-fw fa-google-plus"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a class="btn btn-outline-light btn-social text-center rounded-circle" href="#">
                <i class="fa fa-fw fa-twitter"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a class="btn btn-outline-light btn-social text-center rounded-circle" href="#">
                <i class="fa fa-fw fa-linkedin"></i>
              </a>
            </li>
          </ul>
        </div>
        <div class="col-sm-6">
          <h4 class="text-uppercase mb-4">Acerca de</h4>
          <p class="lead mb-0">
            <a href="http://munaylab.org">MunayLab</a> brinda innovación tecnológica a las Organizaciones de la Sociedad Civil.
          </p>
        </div>
      </div>
    </div>
    <div class="legal text-center">
      <small><b>Copyright &copy; MunayLab 2017</b></small>
    </div>
  </footer>



</body>
</html>
