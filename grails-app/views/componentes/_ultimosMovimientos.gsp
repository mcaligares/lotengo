<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Últimos Movimientos</h3>
  </div>
  <div class="panel-body table-responsive">
    <g:if test="${envios}">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>N°</th>
            <th>Acción</th>
            <th>Fecha</th>
            <th>Destino</th>
            <th>Descripción</th>
          </tr>
        </thead>
        <tbody>
          <g:each in="${envios}" var="envio">
            <tr class="${envio.estado.clase()}">
              <td>${envio.paquete.caja}</td>
              <td>${envio.estado}</td>
              <td>${envio.fecha.format('dd/MM/yyyy')}</td>
              <td>${envio.paquete.destino}</td>
              <td>${envio.paquete.descripcion}</td>
              <td>${envio.responsable.username}</td>
            </tr>
          </g:each>
        </tbody>
      </table>
    </g:if>
    <g:else>
      <div class="text-center">
        <i class="fa fa-table fa-4x" aria-hidden="true"></i>
        <p><i>No hay ningún envio para mostrar</i></p>
      </div>
    </g:else>
  </div>
</div>
