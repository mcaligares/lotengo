<%@ page import="org.munaylab.lotengo.Estado" %>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Buscar Paquete</h3>
  </div>
  <div class="panel-body">
    <g:form controller="gestion" action="buscarPaquete">
      <div class="input-group">
        <input type="number" name="caja" class="form-control" placeholder="N° Caja" required>
        <span class="input-group-btn">
          <button class="btn btn-primary" type="submit">
            <i class="fa fa-search" aria-hidden="true"></i>
          </button>
        </span>
      </div>
    </g:form>
  </div>
</div>

<g:if test="${flash.encontrado}">
  <div class="modal fade" id="encontradoModal" tabindex="-1" role="dialog"
      aria-labelledby="encontradoModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title" id="encontradoModalLabel">
            Paquete N° ${flash.encontrado.paquete.caja} ${flash.encontrado.paquete.estado}
          </h4>
        </div>
        <div class="modal-body">

          <div class="panel panel-default">
            <div class="panel-body">
              <form autocomplete="off">
                <div class="col-sm-3 form-group">
                  <label for="caja">N° Caja</label>
                  <input type="number" class="form-control" id="caja"
                      value="${flash.encontrado.paquete.caja}">
                </div>
                <div class="col-sm-9 form-group">
                  <label for="descripcion">Descripcion</label>
                  <input type="text" class="form-control" id="descripcion"
                      value="${flash.encontrado.paquete.descripcion}">
                </div>
                <div class="col-sm-6 form-group">
                  <label for="origen">Origen</label>
                  <input type="text" class="form-control" id="origen"
                      value="${flash.encontrado.paquete.origen}">
                </div>
                <div class="col-sm-6 form-group">
                  <label for="destino">Destino</label>
                  <input type="text" class="form-control" id="destino"
                      value="${flash.encontrado.paquete.destino}">
                </div>
              </form>
            </div>
            <div class="panel-footer text-right">
              <button type="submit" class="btn btn-primary ">Guardar</button>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Historial</h3>
            </div>
            <div class="panel-body">
              <g:if test="${flash.encontrado.historial}">
                <g:each var="historial" in="${flash.encontrado.historial}">
                  <div class="panel panel-${historial.estado.clase()}">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        Paquete ${historial.estado} el ${historial.fecha.format('dd/MM/yyyy HH:mm')}
                      </h4>
                    </div>
                    <div class="panel-body">
                      <g:if test="${historial.observacion}">
                        <b>Observación:</b>
                        ${historial.observacion}
                      </g:if>
                      <g:else>
                      <div class="text-center">
                        <i>No hay observaciones</i>
                      </div>
                      </g:else>
                    </div>
                  </div>
                </g:each>
              </g:if>
              <g:else>
                <div class="text-center">
                  <i>El paquete está listo para ser enviado</i>
                  <i class="fa fa-check" aria-hidden="true"></i>
                </div>
              </g:else>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript">
    $(document).ready(function() {
      $('#encontradoModal').modal();
    });
  </script>

</g:if>
