<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Entregas Pendientes</h3>
  </div>
  <div class="panel-body table-responsive">
    <g:if test="${envios}">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>N°</th>
            <th>Fecha Envío</th>
            <th>Descripción</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <g:each in="${envios}" var="envio">
            <tr>
              <td>${envio.paquete.caja}</td>
              <td>${envio.fecha.format('dd/MM/yyyy')}</td>
              <td>${envio.paquete.descripcion}</td>
              <td>
                <g:form controller="gestion" action="recibido" id="${envio.paquete.id}">
                  <button type="submit" class="btn btn-primary">Recibido</button>
                </g:form>
              </td>
            </tr>
          </g:each>
        </tbody>
      </table>
    </g:if>
    <g:else>
      <div class="text-center">
        <i class="fa fa-table fa-4x" aria-hidden="true"></i>
        <p><i>No se encontraron paquetes pendientes</i></p>
      </div>
    </g:else>
  </div>
</div>
