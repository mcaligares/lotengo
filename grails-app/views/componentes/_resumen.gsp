<div class="panel panel-default">
  <div class="panel-body">
    <g:each in="${resumen}" var="registro">
      <div class="alert alert-${registro[1].clase()} text-center" role="alert">
        <h3>
          <i class="fa ${registro[1].icono()}"></i>
          <b>${registro[0]} ${registro[1].toString().toLowerCase().capitalize()}s</b>
        </h3>
      </div>
    </g:each>
  </div>
</div>
