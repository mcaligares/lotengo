<g:set var="config" value="${g.configuracionActual()}" />

<script type="text/javascript">
  $(document).ready(function() {
    $('#config').click(function() {
      $('#configModal').modal();
    });
    $('#configForm input[type=checkbox]').change(function() {
      console.log($(this).attr('name'), $(this).is(':checked'));
      $(this).prop('checked', $(this).is(':checked'));
    });
  });
</script>

<div class="modal fade" id="configModal" tabindex="-1" role="dialog" aria-labelledby="configModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="configModalLabel">Configuración</h4>
      </div>
      <g:form name="configForm" controller="gestion" action="configuracion">
        <input type="hidden" name="id" value="${config.id}">
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-6">
              <div class="checkbox">
                <label>
                  <g:checkBox name="resumen" value="${config.resumen}" /> Panel de resumen
                </label>
              </div>
              <div class="checkbox">
                <label>
                  <g:checkBox name="buscador" value="${config.buscador}" /> Búsqueda rápida
                </label>
              </div>
              <div class="checkbox">
                <label>
                  <g:checkBox name="agregarPaquete" value="${config.agregarPaquete}" /> Panel para agregar paquete
                </label>
              </div>
              <div class="checkbox">
                <label>
                  <g:checkBox name="agregarUbicacion" value="${config.agregarUbicacion}" /> Panel para agregar ubicación
                </label>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="checkbox">
                <label>
                  <g:checkBox name="ultimosMovimientos" value="${config.ultimosMovimientos}" />Listado últimos movimientos
                </label>
              </div>
              <div class="checkbox">
                <label>
                  <g:checkBox name="paquetesPendientes" value="${config.paquetesPendientes}" /> Listado paquetes pendientes
                </label>
              </div>
              <div class="checkbox">
                <label>
                  <g:checkBox name="paquetesEnviados" value="${config.paquetesEnviados}" /> Listado paquetes enviados
                </label>
              </div>
              <div class="checkbox">
                <label>
                  <g:checkBox name="paquetesRecibidos" value="${config.paquetesRecibidos}" /> Listado paquetes recibidos
                </label>
              </div>
              <div class="checkbox">
                <label>
                  <g:checkBox name="paquetesPerdidos" value="${config.paquetesPerdidos}" /> Listado paquetes perdidos
                </label>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <button type="submit" class="btn btn-primary">Guardar</button>
        </div>
      </g:form>
    </div>
  </div>
</div>
