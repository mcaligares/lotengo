<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Agregar Paquete</h3>
  </div>
  <div class="panel-body">
    <g:form controller="gestion" action="actualizarPaquete">
      <input type="hidden" name="estado" value="PENDIENTE">
      <div class="form-group">
        <input type="number" class="form-control" name="caja" id="caja" placeholder="N° Caja" required>
      </div>
      <div class="form-group">
        <input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripción">
      </div>
      <div class="form-group">
        <select id="origen" class="form-control" name="origen.id" required>
          <option value="" disabled selected>Origen</option>
          <g:each in="${ubicaciones}" var="ubicacion">
            <option value="${ubicacion.id}">${ubicacion}</option>
          </g:each>
        </select>
      </div>
      <div class="form-group">
        <select id="destino" class="form-control" name="destino.id" required>
          <option value="" disabled selected>Destino</option>
          <g:each in="${ubicaciones}" var="ubicacion">
            <option value="${ubicacion.id}">${ubicacion}</option>
          </g:each>
        </select>
      </div>
      <div class="text-center">
        <button type="submit" class="btn btn-primary">Preparado</button>
      </div>
    </g:form>
  </div>
</div>
