<%@ page import="org.munaylab.lotengo.Estado" %>

<div class="panel panel-${estado.clase()}">
  <div class="panel-heading">
    <h3 class="panel-title">
      <i class="fa ${estado.icono()}" aria-hidden="true"></i>
      Paquetes ${estado.toString().toLowerCase().capitalize()}s
    </h3>
  </div>
  <div class="panel-body table-responsive">
    <g:if test="${paquetes}">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>N°</th>
            <th>Origen</th>
            <th>Destino</th>
            <th>Descripción</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <g:each in="${paquetes}" var="paquete">
            <tr class="${paquete.estado.clase()}">
              <td>${paquete.caja}</td>
              <td>${paquete.origen}</td>
              <td>${paquete.destino}</td>
              <td>${paquete.descripcion}</td>
              <td class="text-right">
                <g:if test="${estado == Estado.PENDIENTE}">
                  <g:form controller="gestion" action="enviar" id="${paquete.id}">
                    <button type="submit" class="btn btn-primary">Enviado</button>
                  </g:form>
                </g:if>
                <g:if test="${estado == Estado.ENVIADO}">
                  <g:form controller="gestion" action="recibido" id="${paquete.id}">
                    <button type="submit" class="btn btn-warning">Recibido</button>
                  </g:form>
                </g:if>
              </td>
            </tr>
          </g:each>
        </tbody>
      </table>
    </g:if>
    <g:else>
      <div class="text-center">
        <i class="fa fa-table fa-4x" aria-hidden="true"></i>
        <p><i>No hay ningún paquete para mostrar</i></p>
      </div>
    </g:else>
  </div>
</div>
