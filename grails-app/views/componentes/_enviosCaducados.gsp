<div class="panel panel-danger">
  <div class="panel-heading">
    <h3 class="panel-title">Envíos Caducados</h3>
  </div>
  <div class="panel-body table-responsive">
    <g:if test="${envios}">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>N°</th>
            <th>Envío</th>
            <th>Origen</th>
            <th>Destino</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <g:each in="${envios}" var="envio">
            <tr>
              <td>${envio.paquete.caja}</td>
              <td>Hace ${groovy.time.TimeCategory.minus(new Date(), envio.fecha).days} días</td>
              <td>${envio.paquete.origen}</td>
              <td>${envio.paquete.destino}</td>
              <td>
                <g:form controller="gestion" action="perdido" id="${envio.paquete.id}">
                  <button type="submit" class="btn btn-danger">Perdido</button>
                </g:form>
              </td>
            </tr>
          </g:each>
        </tbody>
      </table>
    </g:if>
    <g:else>
      <div class="text-center">
        <i class="fa fa-table fa-4x" aria-hidden="true"></i>
        <p><i>No se encontraron paquetes pendientes</i></p>
      </div>
    </g:else>
  </div>
</div>
