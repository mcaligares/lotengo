<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Agregar Ubicación</h3>
  </div>
  <div class="panel-body">
    <g:form controller="gestion" action="actualizarUbicacion">
      <div class="form-group">
        <input type="text" class="form-control" name="provincia" placeholder="Provincia" required>
      </div>
      <div class="form-group">
        <input type="text" class="form-control" name="localidad" placeholder="Localidad" required>
      </div>
      <div class="form-group">
        <input type="text" class="form-control" name="descripcion" placeholder="Descripción">
      </div>
      <div class="text-center">
        <button type="submit" class="btn btn-primary">Agregar</button>
      </div>
    </g:form>
  </div>
</div>
