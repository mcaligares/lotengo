<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Confirmar Entrega</h3>
  </div>
  <div class="panel-body">
    <form name="name" action="action">
      <div class="form-group">
        <label for="caja">Caja</label>
        <input type="text" class="form-control" id="caja" placeholder="N° Caja">
      </div>
      <div class="form-group">
        <label for="descripcion">Descripción</label>
        <input type="text" class="form-control" id="descripcion" placeholder="Descripción">
      </div>
      <div class="form-group">
        <label for="observacion">Observación</label>
        <input type="text" class="form-control" id="observacion" placeholder="Observación">
      </div>
      <button type="submit" class="btn btn-default">Enviar</button>
    </form>
  </div>
</div>
