<g:if test="${flash.result}">
  <div class="alert alert-${flash.result.tipo} alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    ${flash.result.texto}
  </div>
</g:if>

<div class="row">
  <div class="col-sm-9">
    <div class="col-sm-12">
      <g:enviosPendientes/>
    </div>
    <div class="col-sm-12">
      <g:enviosCaducados/>
    </div>
  </div>
  <div class="col-sm-3">
    <g:render template="/componentes/confirmarEntrega"/>
  </div>
</div>
