
<g:if test="${flash.result}">
  <div class="alert alert-${flash.result.tipo} alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    ${flash.result.texto}
  </div>
</g:if>

<g:set var="config" value="${g.configuracionActual()}" />

<div class="row ">
  <div class="col-sm-3">

    <g:if test="${config.resumen}">
      <div class="col-sm-12">
        <g:resumenPaquetes />
      </div>
    </g:if>
    <g:if test="${config.buscador}">
      <div class="col-sm-12">
        <g:render template="/componentes/buscarPaquete"/>
      </div>
    </g:if>
    <g:if test="${config.agregarPaquete}">
      <div class="col-sm-12">
        <g:agregarPaquete/>
      </div>
    </g:if>
    <g:if test="${config.agregarUbicacion}">
      <div class="col-sm-12">
        <g:render template="/componentes/agregarUbicacion"/>
      </div>
    </g:if>
  </div>

  <div class="col-sm-9">
    <g:if test="${config.ultimosMovimientos}">
      <div class="col-sm-12">
        <g:ultimosMovimientos/>
      </div>
    </g:if>
    <g:if test="${config.paquetesPendientes}">
      <div class="col-sm-12">
        <g:ultimosPaquetes estado="pendiente"/>
      </div>
    </g:if>
    <g:if test="${config.paquetesEnviados}">
      <div class="col-sm-12">
        <g:ultimosPaquetes estado="enviado"/>
      </div>
    </g:if>
    <g:if test="${config.paquetesRecibidos}">
      <div class="col-sm-12">
        <g:ultimosPaquetes estado="recibido"/>
      </div>
    </g:if>
    <g:if test="${config.paquetesPerdidos}">
      <div class="col-sm-12">
        <g:ultimosPaquetes estado="perdido"/>
      </div
    </g:if>
  </div>
</div>
