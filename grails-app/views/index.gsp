<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Welcome to Grails</title>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
</head>
<body>

  <sec:ifAllGranted roles='SEDE'>
    <g:render template="/usuario"/>
  </sec:ifAllGranted>

  <sec:ifAllGranted roles='ADMIN'>
    <g:render template="/admin"/>
  </sec:ifAllGranted>

</body>
</html>
