package org.munaylab

import org.munaylab.lotengo.*

class BootStrap {

    def init = { servletContext ->
        Rol rolAdmin = new Rol(authority: 'ADMIN').save()
        Usuario admin = new Usuario(username: 'admin@fundacionsi.org.ar', password: 'admin',
            configuracion: Configuracion.inicial).save()
        new UsuarioRol(usuario: admin, rol: rolAdmin).save()

        Rol rolSede = new Rol(authority: 'SEDE').save()
        Usuario sedeJujuy = new Usuario(sede: 'jujuy', username: 'jujuy@fundacionsi.org.ar', password: 'jujuy',
            configuracion: Configuracion.inicial).save()
        new UsuarioRol(usuario: sedeJujuy, rol: rolSede).save()

        Ubicacion central = new Ubicacion(provincia: 'Bs. As.', localidad: 'Capital', descripcion: 'Deposito Central').save()
        Ubicacion jujuy = new Ubicacion(provincia: 'Jujuy', localidad: 'San Salvador de Jujuy', descripcion: 'Sede').save()
        new Ubicacion(provincia: 'Jujuy', localidad: 'Abra Pampa', descripcion: 'Universidad Warmi').save()
        new Ubicacion(provincia: 'Salta', localidad: 'Salta', descripcion: 'Sede').save()
        new Ubicacion(provincia: 'Salta', localidad: 'Salta', descripcion: 'Residencia').save()

        Paquete p1 = new Paquete(caja: 100, estado: Estado.ENVIADO, origen: central, destino: jujuy, descripcion: 'Telas para titeres').save()
        Paquete p2 = new Paquete(caja: 101, estado: Estado.ENVIADO, origen: central, destino: jujuy, descripcion: 'Hilos para titeres').save()
        Paquete p3 = new Paquete(caja: 102, estado: Estado.ENVIADO, origen: central, destino: jujuy, descripcion: 'Trapos para titeres').save()
        Paquete p4 = new Paquete(caja: 103, estado: Estado.ENVIADO, origen: central, destino: jujuy, descripcion: 'Botones para titeres').save()
        Paquete p5 = new Paquete(caja: 104, estado: Estado.ENVIADO, origen: central, destino: jujuy, descripcion: 'Frazadas para recorridas').save()

        new Historial(fecha: new Date()-100, estado: Estado.ENVIADO, paquete: p1, responsable: admin).save()
        new Historial(fecha: new Date()-50, estado: Estado.ENVIADO, paquete: p2, responsable: admin).save()
        new Historial(fecha: new Date()-31, estado: Estado.ENVIADO, paquete: p3, responsable: admin).save()
        new Historial(fecha: new Date()-20, estado: Estado.ENVIADO, paquete: p4, responsable: admin).save()
        new Historial(fecha: new Date()-10, estado: Estado.ENVIADO, paquete: p5, responsable: admin).save()
    }

    def destroy = {

    }

}
